<a name="1.0.61"></a>
## [1.0.61](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.59...v1.0.61) (2018-02-09)



<a name="1.0.59"></a>
## [1.0.59](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.55...v1.0.59) (2018-02-09)


### Bug Fixes

* add readme ([0978653](https://bitbucket.org/kevinboosten/yarn-publish/commits/0978653))
* tmp 1 ([c3cfbf9](https://bitbucket.org/kevinboosten/yarn-publish/commits/c3cfbf9))



<a name="1.0.55"></a>
## [1.0.55](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.54...v1.0.55) (2018-02-09)



<a name="1.0.54"></a>
## [1.0.54](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.53...v1.0.54) (2018-02-07)



<a name="1.0.53"></a>
## [1.0.53](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.51...v1.0.53) (2018-02-07)



<a name="1.0.52"></a>
## [1.0.52](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.51...v1.0.52) (2018-02-07)



<a name="1.0.51"></a>
## [1.0.51](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.50...v1.0.51) (2018-02-07)



<a name="1.0.50"></a>
## [1.0.50](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.49...v1.0.50) (2018-02-07)



<a name="1.0.49"></a>
## [1.0.49](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.48...v1.0.49) (2018-02-07)



<a name="1.0.48"></a>
## [1.0.48](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.47...v1.0.48) (2018-02-07)



<a name="1.0.47"></a>
## [1.0.47](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.46...v1.0.47) (2018-02-07)



<a name="1.0.46"></a>
## [1.0.46](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.45...v1.0.46) (2018-02-07)



<a name="1.0.45"></a>
## [1.0.45](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.44...v1.0.45) (2018-02-07)



<a name="1.0.44"></a>
## [1.0.44](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.43...v1.0.44) (2018-02-07)



<a name="1.0.43"></a>
## [1.0.43](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.40...v1.0.43) (2018-02-07)



<a name="1.0.40"></a>
## [1.0.40](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.39...v1.0.40) (2018-02-07)



<a name="1.0.39"></a>
## [1.0.39](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.38...v1.0.39) (2018-02-07)



<a name="1.0.38"></a>
## [1.0.38](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.37...v1.0.38) (2018-02-07)



<a name="1.0.37"></a>
## [1.0.37](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.36...v1.0.37) (2018-02-07)



<a name="1.0.36"></a>
## [1.0.36](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.35...v1.0.36) (2018-02-07)



<a name="1.0.35"></a>
## [1.0.35](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.34...v1.0.35) (2018-02-07)



<a name="1.0.34"></a>
## [1.0.34](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.33...v1.0.34) (2018-02-07)



<a name="1.0.33"></a>
## [1.0.33](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.32...v1.0.33) (2018-02-07)



<a name="1.0.32"></a>
## [1.0.32](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.31...v1.0.32) (2018-02-07)



<a name="1.0.31"></a>
## [1.0.31](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.30...v1.0.31) (2018-02-07)



<a name="1.0.30"></a>
## [1.0.30](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.29...v1.0.30) (2018-02-07)


### Features

* **test:** something nice ([0e72940](https://bitbucket.org/kevinboosten/yarn-publish/commits/0e72940))



<a name="1.0.29"></a>
## [1.0.29](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.28...v1.0.29) (2018-02-07)



<a name="1.0.28"></a>
## [1.0.28](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.25...v1.0.28) (2018-02-07)


### Features

* add new stuff ([cd2f2ea](https://bitbucket.org/kevinboosten/yarn-publish/commits/cd2f2ea))



<a name="1.0.27"></a>
## [1.0.27](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.26...v1.0.27) (2018-01-30)



<a name="1.0.26"></a>
## [1.0.26](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.25...v1.0.26) (2018-01-30)


### Features

* add new stuff ([cd2f2ea](https://bitbucket.org/kevinboosten/yarn-publish/commits/cd2f2ea))



<a name="1.0.25"></a>
## [1.0.25](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.24...v1.0.25) (2018-01-28)



<a name="1.0.24"></a>
## [1.0.24](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.23...v1.0.24) (2018-01-28)



<a name="1.0.23"></a>
## [1.0.23](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.22...v1.0.23) (2018-01-28)



<a name="1.0.22"></a>
## [1.0.22](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.21...v1.0.22) (2018-01-28)



<a name="1.0.21"></a>
## [1.0.21](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.9...v1.0.21) (2018-01-28)



<a name="1.0.15"></a>
## [1.0.15](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.12...v1.0.15) (2018-01-25)



<a name="1.0.12"></a>
## [1.0.12](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.9...v1.0.12) (2018-01-25)



<a name="1.0.9"></a>
## [1.0.9](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.8...v1.0.9) (2018-01-25)



<a name="1.0.8"></a>
## [1.0.8](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.7...v1.0.8) (2018-01-25)



<a name="1.0.7"></a>
## [1.0.7](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.6...v1.0.7) (2018-01-25)



<a name="1.0.6"></a>
## [1.0.6](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.5...v1.0.6) (2018-01-25)



<a name="1.0.5"></a>
## [1.0.5](https://bitbucket.org/kevinboosten/yarn-publish/compare/v1.0.1...v1.0.5) (2018-01-25)



